import logo from './logo.svg';
import './App.css';
import { useEffect, useState } from 'react';

function App() {

  const [message, setMessage] = useState('API OFFLINE');

  useEffect(() => {
    callBackendAPI()
      .then(res => setMessage(res?.express))
  }, [])

  const callBackendAPI = async () => {
    const response = await fetch('http://impl-vnext:5000/api');
    const body = await response.json();

    if (response.status !== 200) {
      throw Error(body.message)
    } 
    return body
  }

  return (
    <div className="App">
      <header className="App-header">
        {message}
      </header>
    </div>
  );
}

export default App;
